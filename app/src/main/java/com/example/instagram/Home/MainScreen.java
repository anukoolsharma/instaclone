package com.example.instagram.Home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.instagram.R;
import com.example.instagram.utils.BottomNavViewHelper;
import com.example.instagram.utils.SectionPagerAdapter;
import com.example.instagram.utils.UniversalImageLoader;
import com.google.android.material.tabs.TabLayout;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.nostra13.universalimageloader.core.ImageLoader;

public class MainScreen extends AppCompatActivity {
private static final int Activityno=0;
private Context mContext=MainScreen.this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        initImageLoader();
        setupBottomNavigationView();
        setupViewPager();
    }
    private void setupBottomNavigationView(){

        BottomNavigationViewEx bottomNavigationViewEx = (BottomNavigationViewEx) findViewById(R.id.bottomNavviewbar);
        BottomNavViewHelper.setupbottomnavview(bottomNavigationViewEx);
BottomNavViewHelper.enableNavigation(MainScreen.this,bottomNavigationViewEx);

        Menu menu=bottomNavigationViewEx.getMenu();
        MenuItem menuitem=menu.getItem(Activityno);
        menuitem.setChecked(true);


    }

    private void initImageLoader(){
        UniversalImageLoader universalImageLoader = new UniversalImageLoader(mContext);
        ImageLoader.getInstance().init(universalImageLoader.getConfig());
    }
    private void setupViewPager(){
        SectionPagerAdapter adapter = new SectionPagerAdapter(getSupportFragmentManager());
      adapter.addFragment(new CameraFreagment()); //index 0
        adapter.addFragment(new HomeFragment()); //index 1
       adapter.addFragment(new MessagFragment()); //index 2
        ViewPager mViewPager=(ViewPager)findViewById(R.id.container);
        mViewPager.setAdapter(adapter);


       TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
       tabLayout.setupWithViewPager(mViewPager);

       tabLayout.getTabAt(0).setIcon(R.drawable.ic_camera);
        tabLayout.getTabAt(1).setText("GECA   ");
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_arrow);
    }
}
