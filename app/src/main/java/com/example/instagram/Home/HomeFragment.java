package com.example.instagram.Home;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.example.instagram.Models.Photo;
import com.example.instagram.R;
import com.example.instagram.utils.MainfeedListAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class HomeFragment extends Fragment {
FirebaseDatabase dataphoto;
ListView photolist;
Context ext;
List<Photo> photos;
Activity mContext;
    private DatabaseReference myRef;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mainscreen, container, false);
        Toast.makeText(getActivity(),"Welcome to fragments", Toast.LENGTH_SHORT).show();
dataphoto= FirebaseDatabase.getInstance();
myRef=dataphoto.getReference().child("photos");
mContext=getActivity();

photolist=(ListView)view.findViewById(R.id.listView);
photos=new ArrayList<>();


myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                photos.clear();

                for(DataSnapshot photoSnapshot: dataSnapshot.getChildren())
                {
                    Toast.makeText(getActivity(),"got in database", Toast.LENGTH_SHORT).show();
                    Photo photo=photoSnapshot.getValue(Photo.class);
                    photos.add(photo);
                }
                MainfeedListAdapter adapter=new MainfeedListAdapter(mContext,photos);
           photolist.setAdapter(adapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });







    return view;
    }

}
