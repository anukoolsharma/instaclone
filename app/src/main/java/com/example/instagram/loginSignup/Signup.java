package com.example.instagram.loginSignup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.instagram.Home.CameraFreagment;
import com.example.instagram.Home.HomeFragment;
import com.example.instagram.Home.MainScreen;
import com.example.instagram.Home.MessagFragment;
import com.example.instagram.R;
import com.example.instagram.utils.SectionPagerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

public class Signup extends AppCompatActivity {
    FirebaseAuth mAuth;
    ViewPager mViewPager;
    private TextView ET_signupname, ET_signuppass, TV_login;
    private Button B_signup;
    private String email, password;
    private FirebaseMethods firebaseMethods;
    private Context mContext;
    String codesent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);



        mAuth = FirebaseAuth.getInstance();

        setupViewPager();


        TV_login = (TextView) findViewById(R.id.TV_login);


        TV_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Signup.this, login.class);
                startActivity(i);
            }
        });


    }

    private void setupViewPager() {
        SectionPagerAdapter adapter = new SectionPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new SignupWithEmailFrag()); //index 0
        adapter.addFragment(new SignupWithPhoneFrag()); //index 1
        adapter.addFragment(new SignupPhoneOtpFrag());//index 2
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(adapter);


        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        tabLayout.getTabAt(0).setText("Email");

        tabLayout.getTabAt(1).setText("Phone");
    }

    public void creatUser(String email, String password) {

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(Signup.this, "user created", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(Signup.this, "user already exist", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    private void verifySignInCode(String code) {

        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(codesent, code);
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            //here you can open new activity
                            Toast.makeText(getApplicationContext(),
                                    "Login Successfull", Toast.LENGTH_LONG).show();
                        } else {
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                Toast.makeText(getApplicationContext(),
                                        "Incorrect Verification Code ", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });
    }


    public void creatUserPhone(String num) {
        Toast.makeText(getApplicationContext(),
                num, Toast.LENGTH_LONG).show();
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                num, 60, TimeUnit.SECONDS, this, mCallbacks
        );
    }


    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
        }
        @Override
        public void onVerificationFailed(FirebaseException e) {

            Toast.makeText(getApplicationContext(),
                    e.getMessage(), Toast.LENGTH_LONG).show();
        }
        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            codesent = s;
            Toast.makeText(getApplicationContext(),
                    codesent, Toast.LENGTH_LONG).show();
        }
    };
}
