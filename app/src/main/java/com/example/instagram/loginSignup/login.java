package com.example.instagram.loginSignup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.instagram.MainActivity;
import com.example.instagram.R;
import com.example.instagram.Home.MainScreen;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class login extends AppCompatActivity implements View.OnClickListener {
FirebaseAuth mAuth;
private TextView TV_signup;
private Button B_login;
private EditText ET_name,ET_pass;
private String email,password;
    private FirebaseMethods firebaseMethods;
   private Context mContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
mAuth=FirebaseAuth.getInstance();
        ET_name=(EditText)findViewById(R.id.ET_name);
        ET_pass=(EditText)findViewById(R.id.ET_pass);

B_login=(Button)findViewById(R.id.B_login);
TV_signup=(TextView)findViewById(R.id.TV_signup);

B_login.setOnClickListener(this);
TV_signup.setOnClickListener(this);
        firebaseMethods=new FirebaseMethods(mContext);


       // mAuth.addAuthStateListener(function(user));
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.B_login) {

            email= ET_name.getText().toString();
            password= ET_pass.getText().toString();


            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
 Toast.makeText(login.this, "Authentication sucessful",
                                        Toast.LENGTH_SHORT).show();

                                Intent i=new Intent(login.this, MainScreen.class);
                                startActivity(i);


                            } else {
                                // If sign in fails, display a message to the user.

                                Toast.makeText(login.this, "Authentication failed.",
                                        Toast.LENGTH_SHORT).show();

                            }


                        }
                    });

            //delet this line
            firebaseMethods.addNewUser(email, "anikool", "himanshi", "", "","");

        }
        if(v.getId()==R.id.TV_signup)
        {
            Intent i=new Intent(login.this, Signup.class);
            startActivity(i);
        }

    }
}
