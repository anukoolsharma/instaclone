package com.example.instagram.loginSignup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.instagram.Home.MainScreen;
import com.example.instagram.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

public class SignupWithEmailFrag extends Fragment implements View.OnClickListener {


    FirebaseAuth mAuth;
    private TextView ET_signupname, ET_signuppass;
    private Button B_signup;
    private String email, password;
    private FirebaseMethods firebaseMethods;
    private Context mContext;
    int getstate;
    private Signup signup;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_signup_email, container, false);

        mContext = getActivity();

        ET_signupname = (TextView) view.findViewById(R.id.ET_signupname);

        ET_signuppass = (TextView) view.findViewById(R.id.ET_signuppass);
        B_signup = (Button) view.findViewById(R.id.B_signup);


        B_signup.setOnClickListener(this);

        Toast.makeText(mContext,"into frags",Toast.LENGTH_SHORT).show();
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        mContext = getActivity();
    }

    @Override
    public void onClick(View v) {


        if (v.getId() == R.id.B_signup) {

            email = ET_signupname.getText().toString();
            password = ET_signuppass.getText().toString();


            if (mContext != null) {
                Toast.makeText(mContext,"context not null",Toast.LENGTH_SHORT).show();

                ((Signup) mContext).creatUser(email, password);
            }


        }

    }
}
