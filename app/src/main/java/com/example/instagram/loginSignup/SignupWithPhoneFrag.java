package com.example.instagram.loginSignup;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.instagram.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class SignupWithPhoneFrag extends Fragment implements View.OnClickListener {
    private EditText tv_number;
    private Button btn_signup_phone;
    private String number="";
    private Context mContext;
int a;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_signup_phone, container, false);
        init(view);
        setListeners();
        return view;
    }

    private void init(View view) {
        tv_number = (EditText) view.findViewById(R.id.tv_number);
        btn_signup_phone = (Button) view.findViewById(R.id.btn_signup_phone);
    }

    private void setListeners() {
        btn_signup_phone.setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mContext = getActivity();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_signup_phone:
                number = "+91"+tv_number.getText().toString();
                if (mContext != null) {
                    ((Signup) mContext).creatUserPhone(number);
                }
                break;
        }
    }
}
