package com.example.instagram.share;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.instagram.R;
import com.example.instagram.utils.BottomNavViewHelper;
import com.example.instagram.utils.SectionPagerAdapter;
import com.google.android.material.tabs.TabLayout;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;



import com.example.instagram.utils.Permission;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.ViewPager;


public class ShareActivity extends AppCompatActivity {

        private static final int Activityno=2;
    private static final int VERIFY_PERMISSIONS_REQUEST = 1;
private ViewPager mViewPager;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.actvity_share);
            if(checkPermissionsArray(Permission.PERMISSIONS)){
                setupViewPager();
            }else{
                verifyPermissions(Permission.PERMISSIONS);
            }
        }
    private void setupViewPager(){
       SectionPagerAdapter adapter =  new SectionPagerAdapter(getSupportFragmentManager());
       adapter.addFragment(new GalleryFragment());
       adapter.addFragment(new PhotoFragment());

      mViewPager = (ViewPager) findViewById(R.id.container);
       mViewPager.setAdapter(adapter);

       TabLayout tabLayout = (TabLayout) findViewById(R.id.tabsBottom);
      tabLayout.setupWithViewPager(mViewPager);

     tabLayout.getTabAt(0).setText("gallery");
     tabLayout.getTabAt(1).setText("photo");

    }

        public void verifyPermissions(String[] permissions){


            ActivityCompat.requestPermissions(
                    ShareActivity.this,
                    permissions,
                    VERIFY_PERMISSIONS_REQUEST
            );
        }

    public boolean checkPermissions(String permission){

        int permissionRequest = ActivityCompat.checkSelfPermission(ShareActivity.this, permission);

        if(permissionRequest != PackageManager.PERMISSION_GRANTED){

            return false;
        }
        else{

            return true;
        }
    }
    public int getTask(){

        return getIntent().getFlags();
    }

    /**
     * return the current tab number
     * 0 = GalleryFragment
     * 1 = PhotoFragment
     * @return
     */
    public int getCurrentTabNumber(){
        return mViewPager.getCurrentItem();
    }
    public boolean checkPermissionsArray(String[] permissions){


        for(int i = 0; i< permissions.length; i++){
            String check = permissions[i];
            if(!checkPermissions(check)){
                return false;
            }
        }
        return true;
    }

        private void setupBottomNavigationView(){

            BottomNavigationViewEx bottomNavigationViewEx = (BottomNavigationViewEx) findViewById(R.id.bottomNavviewbar);
            BottomNavViewHelper.setupbottomnavview(bottomNavigationViewEx);
            BottomNavViewHelper.enableNavigation(ShareActivity.this,bottomNavigationViewEx);

       Menu menu=bottomNavigationViewEx.getMenu();
            MenuItem menuitem=menu.getItem(Activityno);
            menuitem.setChecked(true);


        }
    }


