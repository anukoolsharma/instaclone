package com.example.instagram.share;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.example.instagram.R;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class PhotoFragment extends Fragment {
    private static final int PHOTO_FRAGMENT_NUM = 1;
    private static final int GALLERY_FRAGMENT_NUM = 2;
    private static final int  CAMERA_REQUEST_CODE = 5;

private GalleryFragment img;
private ImageView img1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photo, container, false);
        Button btnLaunchCamera = (Button) view.findViewById(R.id.btnLaunchCamera);
img1=(ImageView)view.findViewById(R.id.img1);
        img=new GalleryFragment();
btnLaunchCamera.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST_CODE);
    }
});


        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
      if(requestCode==CAMERA_REQUEST_CODE&&resultCode== Activity.RESULT_OK)
      {
          Bitmap photo=(Bitmap)data.getExtras().get("data");
       img1.setImageBitmap(photo);
      }
    }
}
