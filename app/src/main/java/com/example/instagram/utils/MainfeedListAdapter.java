package com.example.instagram.utils;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.instagram.Models.Photo;
import com.example.instagram.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


public class MainfeedListAdapter extends ArrayAdapter<Photo> {
private Activity context;
Context ext;
private List<Photo> photolist;


public MainfeedListAdapter(Activity context,List<Photo> photolist)
{ super(context, R.layout.layout_mainfeed_listitem,photolist);

this.context=context;
this.photolist=photolist;
}


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View ListViewItem=inflater.inflate(R.layout.layout_mainfeed_listitem,null,true);
        ImageView img=(ImageView)ListViewItem.findViewById(R.id.post);
        Photo photo=photolist.get(position);
        String url=photo.getImage_path();

      //   Toast.makeText(getContext(),url, Toast.LENGTH_SHORT).show();
        UniversalImageLoader.setImage(url,img,null,"");
        return ListViewItem;

    }
}

