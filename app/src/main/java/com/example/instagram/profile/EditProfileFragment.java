package com.example.instagram.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.instagram.R;
import com.example.instagram.loginSignup.FirebaseMethods;
import com.example.instagram.share.ShareActivity;
import com.example.instagram.utils.UniversalImageLoader;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class EditProfileFragment extends Fragment {
  EditText username,display_name,website,description,email, phoneNumber;

  ImageView saveChanges,profile_photo;

    private FirebaseMethods firebaseMethods;
    private Context mContext;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_editprofile,container,false);

        username=(EditText)view.findViewById(R.id.username);

        display_name=(EditText)view.findViewById(R.id.display_name);
    website=(EditText)view.findViewById(R.id.website);
      description=(EditText)view.findViewById(R.id.description);
        email=(EditText)view.findViewById(R.id.email);
       phoneNumber=(EditText)view.findViewById(R.id.phoneNumber);
        saveChanges=(ImageView)view.findViewById(R.id.saveChanges);
        profile_photo=(ImageView)view.findViewById(R.id.profile_photo);
        firebaseMethods=new FirebaseMethods(mContext);
AccountSettingsActivity img=new AccountSettingsActivity();
        saveChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email_1=email.getText().toString();
                String username_1=username.getText().toString();
                String description_1=description.getText().toString();
                String website_1=website.getText().toString();
                String phoneNumber_1=phoneNumber.getText().toString();


                firebaseMethods.addNewUser(email_1,username_1,description_1,website_1,"",phoneNumber_1);
            }
        });






        profile_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), ShareActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); //268435456
                getActivity().startActivity(intent);
                getActivity().finish();
            }
        });



        return view;







    }

}
