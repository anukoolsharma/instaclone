package com.example.instagram.profile;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.instagram.Models.User;
import com.example.instagram.Models.UserAccountSettings;
import com.example.instagram.Models.UserSettings;
import com.example.instagram.R;
import com.example.instagram.loginSignup.FirebaseMethods;
import com.example.instagram.utils.BottomNavViewHelper;
import com.example.instagram.utils.UniversalImageLoader;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import androidx.annotation.NonNull;
import  androidx.appcompat.widget.Toolbar;
import androidx.appcompat.app.AppCompatActivity;

public class ProfileActivity extends AppCompatActivity {
    private static final int Activityno=4;
    private DatabaseReference mGetReference;
    private FirebaseDatabase mDatabase;
    private FirebaseMethods mFirebaseMethods;
    //widgets
    private TextView mPosts, mFollowers, mFollowing, mDisplayName, mUsername, mWebsite, mDescription;

  private ImageView mProfilePhoto;
    private GridView gridView;
    private Toolbar toolbar;
    private ImageView profileMenu;
    private BottomNavigationViewEx bottomNavigationView;
private Context mContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
      setupBottomNavigationView();
        setupToolbar();
mFirebaseMethods=new FirebaseMethods(mContext);


        mDisplayName = (TextView)findViewById(R.id.display_name);
        mUsername = (TextView)findViewById(R.id.username);
        mWebsite = (TextView)findViewById(R.id.website);
        mDescription = (TextView)findViewById(R.id.description);
        mProfilePhoto = (ImageView)findViewById(R.id.profile_photo);
        mPosts = (TextView)findViewById(R.id.tvPosts);
        mFollowers = (TextView)findViewById(R.id.tvFollowers);
        mFollowing = (TextView)findViewById(R.id.tvFollowing);

        gridView = (GridView)findViewById(R.id.gridView);
        toolbar = (Toolbar)findViewById(R.id.profileToolBar);
        profileMenu = (ImageView)findViewById(R.id.profileMenu);

   //     mContext = getActivity();




















        mDatabase = FirebaseDatabase.getInstance();

         mGetReference = mDatabase.getReference();

         mGetReference.addValueEventListener(new ValueEventListener() {
             @Override
             public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                 setProfileWidgets(mFirebaseMethods.getUserSettings(dataSnapshot));

             }

             @Override
             public void onCancelled(@NonNull DatabaseError databaseError) {

             }
         });

    }




    private void setupBottomNavigationView(){

        BottomNavigationViewEx bottomNavigationViewEx = (BottomNavigationViewEx) findViewById(R.id.bottomNavviewbar);
        BottomNavViewHelper.setupbottomnavview(bottomNavigationViewEx);
        BottomNavViewHelper.enableNavigation(ProfileActivity.this,bottomNavigationViewEx);

        Menu menu=bottomNavigationViewEx.getMenu();
        MenuItem menuitem=menu.getItem(Activityno);
        menuitem.setChecked(true);


    }



    private void setupToolbar()
    {

        Toolbar toolbar=(Toolbar)findViewById(R.id.profileToolBar);
setSupportActionBar(toolbar);

        ImageView profileMenu=(ImageView)findViewById(R.id.profileMenu);
        profileMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             Intent i=new Intent(ProfileActivity.this,AccountSettingsActivity.class);
             startActivity(i);
            }
        });

    }


    private void setProfileWidgets(UserSettings userSettings) {



     User user = userSettings.getUser();
        UserAccountSettings settings = userSettings.getSettings();

        UniversalImageLoader.setImage(settings.getProfile_photo(), mProfilePhoto, null, "");



        mDisplayName.setText(settings.getDisplay_name());
        mUsername.setText(settings.getUsername());
        mWebsite.setText(settings.getWebsite());
        mDescription.setText(settings.getDescription());

    }

}


