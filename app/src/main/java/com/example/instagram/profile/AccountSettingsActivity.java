package com.example.instagram.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.instagram.R;
import com.example.instagram.loginSignup.FirebaseMethods;
import com.example.instagram.utils.SectionsStatePagerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

public class AccountSettingsActivity extends AppCompatActivity {
private Context mContext;
private SectionsStatePagerAdapter pagerAdapter;
private ViewPager mViewPager;
private RelativeLayout mRelativeLayout;

    private DatabaseReference myRef;
    private FirebaseDatabase mFirebaseDatabase;
    private FirebaseAuth mAuth;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accountsettings);
        mContext = AccountSettingsActivity.this;
        mViewPager = (ViewPager) findViewById(R.id.container);
        mRelativeLayout = (RelativeLayout) findViewById(R.id.relLayout1);
        setupSettingsList();
        setupFragments();
        getIncomingIntent();
        ImageView backArrow=(ImageView)findViewById(R.id.backArrow);
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }



    private void getIncomingIntent() {
        Intent intent = getIntent();
        Toast.makeText(mContext, "till here", Toast.LENGTH_SHORT).show();
        if (intent.hasExtra("selected_image"))
                 {
            if (intent.getStringExtra("return_to_fragment").equals("Edit Profile")) {


                    FirebaseMethods firebaseMethods = new FirebaseMethods(AccountSettingsActivity.this);
                    firebaseMethods.uploadNewPhoto("profile_photo", null, 0,intent.getStringExtra("selected_image"),null);


                }

            }

        }



public void showProfile(String ImgUrl)
{


}











    private void setupFragments()
    {
        pagerAdapter = new SectionsStatePagerAdapter(getSupportFragmentManager());
        pagerAdapter.addFragment(new EditProfileFragment(),"edit_profile_fragment"); //fragment 0
        pagerAdapter.addFragment(new SignOutFragment(), "sign_out_fragment"); //fragment 1
    }




    public void setViewPager(int fragmentNumber){
        mRelativeLayout.setVisibility(View.GONE);

        mViewPager.setAdapter(pagerAdapter);
        mViewPager.setCurrentItem(fragmentNumber);
    }










    private void setupSettingsList(){

        ListView listView = (ListView) findViewById(R.id.lvAccountSettings);

        ArrayList<String> options = new ArrayList<>();
        options.add("Edit Profile"); //fragment 0
        options.add("Sign Out"); //fragement 1

        ArrayAdapter adapter = new ArrayAdapter(mContext, android.R.layout.simple_list_item_1, options);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
setViewPager(position);
            }
        });

    }
}
